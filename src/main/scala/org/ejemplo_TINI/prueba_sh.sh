#!/bin/bash
# ---------------------------------------------------------------------------
# OCEAN
# ---------------------------------------------------------------------------
#
#
# Description of script
# datacopy_sid.sh
# Script transfers data files from a source server ("Anlieferserver") to the local file system
# input : 5 arguments (see below)
# output: return code 0 .. ok
#                     1 .. missing arguments
#
# test run:
# sh datacopy_sid.sh om /ocean/data/om/datasources/ /ocean/application/om/etl-base/etl-trans/bash/scripts/datacopy/ /ocean/data/om/ /ocean/application/om/etl-base/etl-trans/bash/scripts/datacopy/log/
#
# ---------------------------------------------------------------------------
# datacopy_sid.sh  prod_cb            /datasources/               /ocean/application/prod_cb/etl-base/etl-trans/bash/scripts/datacopy/  /ocean/data/prod_cb/     /ocean/application/prod_cb/etl-base/etl-trans/bash/scripts/datacopy/log/
#                  <ENVIRONMENT>       <SRC_DATA_FOLDER>           <DATACOPY_PARAM_FOLDER>                                               <OCEAN_DATA_FOLDER>      <OCEAN_LOG_FOLDER>
#testing
#ENV=acc
##parameters
#ENV_PARAM=dev
#SRC_DATA_FOLDER=/ocean/data/$ENV/datasources/
#DATACOPY_PARAM_FOLDER=/ocean/application/$ENV/etl-base/etl-trans/bash/scripts/datacopy
#OCEAN_DATA_FOLDER=/ocean/data/$ENV/
#OCEAN_LOG_FOLDER=/ocean/application/$ENV/etl-base/etl-trans/bash/scripts/datacopy/log
#./datacopy_sid.sh $ENV_PARAM $SRC_DATA_FOLDER $DATACOPY_PARAM_FOLDER $OCEAN_DATA_FOLDER $OCEAN_LOG_FOLDER
#
[ "$#" -lt "5" ] && { echo "datacopy_sid.sh: arguments missing: ./datacopy_sid.sh <ENVIRONMENT> <SRC_DATA_FOLDER> <DATACOPY_PARAM_FOLDER> <OCEAN_DATA_FOLDER> <OCEAN_LOG_FOLDER>"; exit 1; }

ENVIRONMENT=$1
SRC_DATA_FOLDER=$2
DATACOPY_PARAM_FOLDER=$3
OCEAN_DATA_FOLDER=$4
OCEAN_LOG_FOLDER=$5

#ssh_login=$SRC_DATA_CONNECTION
quellsystem=$SRC_DATA_FOLDER
app_pfad=$DATACOPY_PARAM_FOLDER
zielsystem=$OCEAN_DATA_FOLDER
log_pfad=$OCEAN_LOG_FOLDER

v_LOG_TIME=$(date +"%Y%m%d %H%M")
log_name1="${log_pfad}/${v_LOG_TIME}_log_sftp.log"

## loop through parameter file
echo "##### datacopy_sid.sh #####"

#echo "ENVIRONMENT:" ${ENVIRONMENT} >> ${log_name1}
#echo "SRC_DATA_FOLDER" ${SRC_DATA_FOLDER} >> ${log_name1}
#echo "DATACOPY_PARAM_FOLDER:" ${DATACOPY_PARAM_FOLDER} >> ${log_name1}
#echo "OCEAN_DATA_FOLDER:" ${OCEAN_DATA_FOLDER} >> ${log_name1}
#echo "OCEAN_LOG_FOLDER" ${OCEAN_LOG_FOLDER} >> ${log_name1}
#echo "quellsystem:" ${quellsystem} >> ${log_name1}
#echo "app_pfad:" ${app_pfad} >> ${log_name1}
#echo "zielsystem" ${zielsystem} >> ${log_name1}
#echo "log_pfad:" ${log_pfad} >> ${log_name1}

#cat $app_pfad/datacopy_sid.param >> ${log_name1}
echo "##### datacopy_sid.sh #####"
cat $app_pfad/datacopy_sid.param | (
#            1	anlieferungsserver acs	        acs/	acs/	 acs-das-device_*.gz	2	upload/upload_target/ upload/          archive/
while read aktiv ssh_login         system       pfad    trg_pfad filepattern         archiv	upload_dir_src        upload_dir_dest  archive_dir_src
do
	echo $aktiv, $ssh_login, $system, $pfad, $trg_pfad, $filepattern, $archiv, $upload_dir_src, $upload_dir_dest, $archive_dir_src, `date +"%d.%m.%Y %H:%M"`
	# check if "system" is empty
	if [ "$system" == "" ]; then
		# next System
		continue
	fi
	# check if "system" is activated
	if [ $aktiv -eq 0 ]; then
		# next System
		continue
	fi
	# check if "pfad" is populated
	if [ $pfad == "" ]; then
		# next System
		date; echo 'ERROR parameter path is not set - skipping system ' $system;
		continue
	fi
	if [ $upload_dir_src == "" ]; then
		# next System
		date; echo 'INFO parameter upload_dir_src is not set - assuming default upload/ for system '$system;
		upload_dir_src=upload/
	fi
	if [ $upload_dir_dest == "" ]; then
		# next System
		date; echo 'INFO parameter upload_dir_dest is not set - assuming default upload/ for system '$system;
		upload_dir_dest=upload/
	fi
	if [ $archive_dir_src == "" ]; then
		# next System
		date; echo 'INFO parameter upload_dir_src is not set - assuming default archive/ for system '$system;
		archive_dir_src=archive/
	fi

	# get current timestamp
	datetime=`date +"%Y%m%d"`

	# create logfile name
	log_name=$log_pfad/$datetime'_'$system'-copy.log'
	#log_name="${log_pfad}/${v_LOG_TIME}_log_sftp.log"

	# create filelist name
	filelist_name=$zielsystem$trg_pfad$upload_dir_dest$system-filelist.ls

	#echo $filelist_name >> $log_name
	if [ -f $filelist_name ]; then
		rm $filelist_name
	fi
	# add filenames of new files in source folder to filelist ??? Why here different ase in VoIP?
	#echo "ssh -n $ssh_login cd $quellsystem$pfad$upload_dir_src; find . -type f -name '$filepattern' | xargs -I {} basename {}" >> $log_name
	ssh -n $ssh_login "cd $quellsystem$pfad$upload_dir_src; find . -type f -name '$filepattern' | xargs -I {} basename {}" > $filelist_name

	# loop through filelist
	cat $filelist_name | (
	while read datei
	do
		copyFlag=0; prodFlag=0

		# echo $datei
		# check if file is already in target folder
		if [ -f $zielsystem$trg_pfad$upload_dir_dest$datei ]; then
			# file already there, check file size
			dateiA=`ssh -n $ssh_login ls -al $quellsystem$pfad$upload_dir_src$datei | awk '{print $5}'`
			dateiB=`ls -al $zielsystem$trg_pfad$upload_dir_dest$datei | awk '{print $5}'`
			# if filesizes differ copy file again
			if  [ $dateiA -ne $dateiB ]; then
				echo Filesizes are different, copy again >> $log_name
				copyFlag=1
			fi
		else
			#echo $log_pfad'*_'$system'-copy.log' >> $log_name
			# Check if there is a logfile
			logfilename=$log_pfad'/*_'$system'-copy.log'
			logfiles=$(ls $logfilename 2> /dev/null | wc -l)
			if [ "$logfiles" != "0" ]; then
				# Check if file is already in logfile
				logFlag=`grep -l $datei $logfilename | wc -l`
				if  [ $logFlag -eq 0 ]; then
					# File is not in logfile, copy it
					copyFlag=1
				fi
			else
				copyFlag=1
			fi
		fi

#		copyFlag=1
		# If copyFlag is set -> copy and write logfile
		if  [ $copyFlag -eq 1 ]; then
			# Kopiere Pilot -> Prod

			# copy file in temporary file
			tmpname=`date +"%Y%m%d_%H_%M_%S_%3N"`
			scp $ssh_login':'$quellsystem$pfad$upload_dir_src$datei $zielsystem$trg_pfad$upload_dir_dest'tmp.'${tmpname}

			# after transfer rename file zu origin name
			mv $zielsystem$trg_pfad$upload_dir_dest'tmp.'${tmpname} $zielsystem$trg_pfad$upload_dir_dest$datei
			# successful?
			if [ $? -eq 0 ]; then
				# Check filesize

				dateiA=0
				dateiA=`ssh -n $ssh_login ls -al $quellsystem$pfad$upload_dir_src$datei | awk '{print $5}'`

				if [ -f $zielsystem$trg_pfad$upload_dir_dest$datei ]; then
					dateiB=`ls -al $zielsystem$trg_pfad$upload_dir_dest$datei | awk '{print $5}'`
				else
					dateiB=0
				fi
				#echo $dateiA
				#echo $dateiB
				if [ $dateiA -eq $dateiB ]; then
					prodFlag=1
				else
					prodFlag=0
				fi
				# copied file is fine
			else
				echo Error while copying, no logfile entry
				prodFlag=0
			fi
		fi

		# If both copy operations went fine, write logfile entry
		if [ $prodFlag -eq 1 ]; then
			mydate=$(date +"%Y%m%d %H%M")
			myda=$(date +"%Y%m%d")

			# archiving
			[ $archiv -eq 0 ] && ssh -n $ssh_login rm $quellsystem$pfad$upload_dir_src$datei
			[ $archiv -eq 1 ] && ssh -n $ssh_login mv $quellsystem$pfad$upload_dir_src$datei $quellsystem$pfad$archive_dir_src$datei
			[ $archiv -eq 2 ] && echo keep file
			echo $mydate , $datei, $dateiA, Datei archiviert: $archiv >> $log_name
		fi
	done
	)
	# clean logfile
	find "$log_pfad" -name "*_$system-housekeeper.log" |
	while read fullname;
	do
		I=`basename "$fullname"`
		YYYY=${I:0:4}; MM=${I:4:2}; DD=${I:6:2}
		# trim leading "0" for single digit Day/Month
		DD=${DD##0}; MM=${MM##0}
		d_datei=`perl -le 'use Time::Local; print timelocal(0,0,0,'$DD','$MM-1','$YYYY');'`
		DD=`date +"%d"`; 	DD=${DD##0}
		MM=`date +"%m"`; 	MM=${MM##0}
		YYYY=`date +"%Y"`
		#echo "Parameter " $YYYY $MM $DD
		d_archiv=`perl -le 'use Time::Local; print timelocal(0,0,0,'$DD','$MM-1','$YYYY');'`
		date_diff=$(((d_archiv-d_datei)/86400))
		#echo $I
		if [ $date_diff -gt 20 -a $date_diff -lt 200 ]; then
			# Delete file
			echo deleting
			rm $log_pfad$I
			# write log entry
		fi
	done

       # clean logfile -copy.log
	find "$log_pfad" -name "*_$system-copy.log" |
	while read fullname;
	do
		I=`basename "$fullname"`
		YYYY=${I:0:4}; MM=${I:4:2}; DD=${I:6:2}
		# trim leading "0" for single digit Day/Month
		DD=${DD##0}; MM=${MM##0}
		d_datei=`perl -le 'use Time::Local; print timelocal(0,0,0,'$DD','$MM-1','$YYYY');'`
		DD=`date +"%d"`; 	DD=${DD##0}
		MM=`date +"%m"`; 	MM=${MM##0}
		YYYY=`date +"%Y"`
		#echo "Parameter " $YYYY $MM $DD
		d_archiv=`perl -le 'use Time::Local; print timelocal(0,0,0,'$DD','$MM-1','$YYYY');'`
		date_diff=$(((d_archiv-d_datei)/86400))
		#echo $I
		if [ $date_diff -gt 20 -a $date_diff -lt 200 ]; then
			# Delete file
			rm $log_pfad$I
			# write log entry
		fi
	done
done
)
