package org.ejemplo_TINI
/**
 * @author Faustino
 */

import org.apache.spark.sql.SparkSession
import org.apache.log4j.Logger
import org.apache.log4j.Level

object App {

  def foo(x : Array[String]) = x.foldLeft("")((a,b) => a + b)

  def main(args : Array[String])={
    println( "Primer Ejemplo de Scala con spark!" )
    println("Argumentos= = " + foo(args))

    val spark = SparkSession.builder()
        .master("local[1]")
        .appName("SparkByExample")
        .getOrCreate();
    spark.sparkContext.setLogLevel("ERROR")

    println("First SparkContext:")
    println("APP Name :" + spark.sparkContext.appName);
    println("Deploy Mode :" + spark.sparkContext.deployMode);
    println("Master :" + spark.sparkContext.master);

    // Carga de archivo de viajeros en el Titanic
    val df = spark.read.option("header","true").csv("data_sources/titanic.csv")
    df.show(10,false)
    print("Pasajeros->",df.count())

    // Muertos
    val supervivientes = df.filter(df.col("Survived")==="1")
    print("Supervivientes->",supervivientes.count())
    }

}
